package cn.com.alibaba.controller;

import cn.com.alibaba.service.IAService;
import cn.com.alibaba.service.IBService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 10:48 2020/12/28
 * @author LuoFeng
 */
@RestController
@RequestMapping("servcie-a")
public class ServiceAController {

    @DubboReference(generic = false,check=false,interfaceClass = IBService.class,interfaceName = "cn.com.alibaba.IBService")
    private IBService ibService;

    @DubboReference(generic = false,check=false,interfaceClass = IAService.class,interfaceName = "cn.com.alibaba.IAService")
    private IAService iaService;

    @GetMapping("hello/{name}")
    public String getInfo(@PathVariable String name){
        return this.ibService.getInfo(name);
    }


    @GetMapping("say/{zh}")
    public String sayInfo(@PathVariable String zh) throws Exception {
        if("1".equals(zh)){
            throw new Exception("我已经报错了");
        }
        return this.iaService.getInfo(zh);
    }
}
