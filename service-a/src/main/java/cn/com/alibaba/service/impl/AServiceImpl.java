package cn.com.alibaba.service.impl;

import cn.com.alibaba.fallback.FallBackService;
import cn.com.alibaba.service.IAService;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * 
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 15:13 2020/12/28
 * @author LuoFeng
 */
@DubboService
public class AServiceImpl implements IAService {

    @SentinelResource(value = "getInfo" ,fallback = "sentinelTestFallBackClass",fallbackClass = FallBackService.class)
    @Override
    public String getInfo(String name) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "A-Hello,"+name;
    }
}
