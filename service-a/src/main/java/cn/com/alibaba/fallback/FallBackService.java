package cn.com.alibaba.fallback;

/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 9:23 2020/12/30
 * @author LuoFeng
 */
public class FallBackService {

	/**
	 * 参数要降级之前的方法参数保持一致
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @Param: [name, e]
	 * @return: java.lang.String
	 * @Author: LuoFeng
	 * @Date: 9:23 2020/12/30
	 */
	public static String sentinelTestFallBackClass ( String name,Throwable e ) {
		System.out.println("已经降级处理了:"+e.getMessage());

		e.printStackTrace();
		//可以处理各种类型的异常，自定义异常
		if (e instanceof RuntimeException) {
			System.out.println ("异常类型");
		}
		return name+" Error";
	}
}
