package cn.com.alibaba.controller;

import cn.com.alibaba.service.IAService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 10:48 2020/12/28
 * @author LuoFeng
 */
@RestController
@RequestMapping("servcie-b")
public class ServiceBController {

    @DubboReference(generic = false,check=false,interfaceClass = IAService.class,interfaceName = "cn.com.alibaba.IAService")
    private IAService iaService;

    @GetMapping("hello/{name}")
    public String getInfo(@PathVariable String name){
        return this.iaService.getInfo(name);
    }
}
