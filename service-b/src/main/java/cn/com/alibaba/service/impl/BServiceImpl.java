package cn.com.alibaba.service.impl;

import cn.com.alibaba.service.IBService;
import org.apache.dubbo.config.annotation.DubboService;
/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 15:18 2020/12/28
 * @author LuoFeng
 */
@DubboService
public class BServiceImpl implements IBService {
    @Override
    public String getInfo(String name) {
        return "B-Hello,"+name;
    }
}
