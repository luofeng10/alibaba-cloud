package cn.com.alibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 14:36 2020/11/30
 * @author LuoFeng
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ServiceBApplication {

    public static void main(String[] args){
        SpringApplication.run(ServiceBApplication.class,args);
    }
}
