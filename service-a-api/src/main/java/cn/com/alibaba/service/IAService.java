package cn.com.alibaba.service;


/**
 *
 * @Description: TODO(这里用一句话描述这个方法的作用)
 * @date 15:15 2020/12/28
 * @author LuoFeng
 */
public interface IAService {

    /**
     * 获取信息
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @Param: [name]
     * @return: java.lang.String
     * @Author: LuoFeng
     * @Date: 15:15 2020/12/28
     */
    String getInfo(String name);
}
